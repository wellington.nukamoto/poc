
# Agile Process Optimizer
Projeto Core do sistema. (Melhorar descrição do projeto)

## Início
Essas instruções fornecerão uma cópia do projeto instalado e funcionando em sua máquina local para fins de
desenvolvimento e testes.

### Pré-requisitos
O que será necessário para rodar o projeto para desenvolvimento.

*Caso esteja utilizando: **Windows** se certifique que esteja com o Bitlocker ativo caso seja **Linux** (nota... buscar
ferramenta similar)*

* [Python 2.7.14](https://www.python.org/ftp/python/2.7.14/python-2.7.14.msi) - Linguagem utilizada
* [Git](https://git-scm.com/downloads) - Sistema de controle de versão
* [Docker](https://www.docker.com/) - Cria ambientes isolados
* [Virtualenv](https://pypi.org/project/virtualenv/) - Construtor de ambiente virtual Python
* [MySQL-python 1.2.5](https://pypi.org/project/MySQL-python/#files) - Interface do MySQL para o Python
* [LXML 3.3.3](http://biodev.ece.ucsb.edu:4040/root/pypi/lxml/3.3.3) - Biblioteca de processamento XML
* [Gettext](https://mlocati.github.io/articles/gettext-iconv-windows.html) - Biblioteca que faz a internacionalização

Se estiver com dificuldades na instalação, acessar a documentação para o setup inicial clicando [aqui](link).

### Criação de um novo banco de dados
Para desenvolvimento local poderá ser criado um banco de dados. Para isto, vamos utilizar um container docker.
```
docker run --name=ap-optimizer-db -p 3306:3306  --env="MYSQL_ROOT_PASSWORD=123456" mysql:5.6.40
```

*Após subir o container, importar um dump de um banco de dados existente no container local.*

### Configuração do projeto
Utilizando o git, será possível efetuar o clone do mesmo.
```
git clone https://gitlab.com/agileprocess/developer/trainee/Agile-Process-Optimizer.git
```

Acesse o diretório
```
cd Agile-Process-Optimizer
```

Crie uma virtualenv
```
virtualenv .venv
```

Para ativar a virtualenv no windows
```
call .venv/Scripts/activate
```

Instale as dependências necessárias
```
pip install -r requirements-local.txt
```

Será necessário configurar o banco de dados no atributo *DATABASE_URL* localizado no
arquivo *settings.py*
```
DATABASES = {
    'default': dj_database_url.config(default=os.environ.get('DATABASE_URL', 'mysql://root:123456@localhost:3306/banco_local')),
}
```

Executando o projeto
```
python manage.py runserver 127.0.0.1:8000
```

Acesse no seu browser
```
http://127.0.0.1:8000/
```

## Qualidade
### Testes unitários
Executa UnitTests sem migrações
```
python manage.py test --nomigrations
```

### Convenção de código
Utiliza o flake8 para padronização e convenção do código fonte

```
flake8
```
